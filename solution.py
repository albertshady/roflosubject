import collections
import csv
import pathlib
import random
import typing
from datetime import date, datetime

import matplotlib.pyplot as plt


Category_T = str
MCC_T = int
Categorized_MCCs_T = typing.Mapping[Category_T, typing.Mapping[MCC_T, int]]

N = 65

# TODO: configure it
YOUR_BIRTH_DAY: int = 0
SOURCE_AXIS_TITLE: typing.Optional[str] = 'Source data'
GENERATED_AXIS_TITLE: typing.Optional[str] = "Source + generated data"
X_LABEL: typing.Optional[str] = "Category"
Y_LABEL: typing.Optional[str] = "Number of transactions"
SHOW_LEGEND: bool = True


class Transaction(typing.NamedTuple):
    mcc: int
    trx_category: str


# No inheritance because order matters
class CompleteTransaction(typing.NamedTuple):
    period: date
    cl_id: int
    mcc: int
    channel_type: str
    currency: int
    trdatetime: datetime
    amount: float
    trx_category: str

    @classmethod
    def fieldnames(cls) -> typing.Sequence[str]:
        return cls._fields


class Dataset(typing.NamedTuple):
    transactions: typing.List[Transaction]

    @classmethod
    def from_file(cls, filepath: pathlib.Path, limit: int) -> 'Dataset':
        return cls(
            transactions=list(
                cls._load_required_transactions(filepath=filepath, limit=limit)
            )
        )

    @staticmethod
    def _load_required_transactions(filepath: pathlib.Path, limit: int) -> typing.Iterator[Transaction]:
        with open(filepath, 'r') as file:
            file.readline()  # skip header
            reader = csv.DictReader(file, fieldnames=CompleteTransaction.fieldnames())
            for _ in range(limit):
                kwargs = next(reader)
                yield Transaction(
                    mcc=int(kwargs['mcc']),
                    trx_category=kwargs['trx_category'],
                )

    def generate_new_transaction(self) -> Transaction:
        mccs = [transaction.mcc for transaction in self.transactions]
        mcc = mccs[random.randint(0, len(mccs) - 1)]
        mcc_categories = [transaction.trx_category for transaction in self.transactions if transaction.mcc == mcc]
        category = mcc_categories[random.randint(0, len(mcc_categories) - 1)]

        return Transaction(mcc=mcc, trx_category=category)


class Bar(typing.NamedTuple):
    x: typing.List[str]
    heights: typing.List[int]
    bottom: typing.List[int]
    label: str
    color: str


class MccList(typing.NamedTuple):
    by_category: typing.List[int]
    code: int


class ChartInputData(typing.NamedTuple):
    """Every mccs[y][x] represents number of mccs that belong to labels[x]"""

    labels: typing.List[str]
    mccs: typing.List[MccList]

    def bars(self) -> typing.Iterator[Bar]:
        bottom = [0] * len(self.labels)
        for color_ix, mcc in enumerate(self.mccs):
            yield Bar(
                x=self.labels,
                heights=mcc.by_category,
                bottom=bottom,
                label=str(mcc.code),
                color=COLORS[color_ix],
            )
            bottom = [bottom[x] + mcc.by_category[x] for x in range(len(bottom))]


def main(dataset_file_path: pathlib.Path, your_birth_day: int) -> None:
    dataset_length = N + your_birth_day
    original_dataset = Dataset.from_file(filepath=dataset_file_path, limit=dataset_length)
    generated_transactions = [original_dataset.generate_new_transaction() for _ in range(dataset_length)]
    generated_dataset = Dataset(transactions=original_dataset.transactions + generated_transactions)
    draw_comparison_chart(original_dataset, generated_dataset)


def draw_comparison_chart(original_dataset, generated_dataset) -> None:
    figure, (original_ax, generated_ax) = plt.subplots(ncols=2)

    draw_ax(original_ax, original_dataset)
    draw_ax(generated_ax, generated_dataset)

    if SOURCE_AXIS_TITLE:
        original_ax.set_title(SOURCE_AXIS_TITLE)
    if GENERATED_AXIS_TITLE:
        generated_ax.set_title(GENERATED_AXIS_TITLE)

    plt.show()


def draw_ax(ax, dataset: Dataset) -> None:
    chart_input_data = prepare_chart_input_data(dataset=dataset)

    for bar in chart_input_data.bars():
        ax.bar(x=bar.x, height=bar.heights, bottom=bar.bottom, label=bar.label, color=bar.color)

    if X_LABEL:
        ax.set_xlabel(X_LABEL)
    if Y_LABEL:
        ax.set_ylabel(Y_LABEL)
    if SHOW_LEGEND:
        ax.legend()


def prepare_chart_input_data(dataset: Dataset) -> ChartInputData:
    categorized_mccs = categorize_mccs(dataset)
    all_mccs = {transaction.mcc for transaction in dataset.transactions}

    categories: typing.List[Category_T] = []
    mcc_lists: typing.List[MccList] = []

    for category, mcc_occurrencies in categorized_mccs.items():
        categories.append(category)
        for i, mcc in enumerate(all_mccs):
            occurrencies_in_category = mcc_occurrencies[mcc]
            try:
                mcc_lists[i].by_category.append(occurrencies_in_category)
            except IndexError:
                mcc_lists.append(MccList(code=mcc, by_category=[occurrencies_in_category]))

    return ChartInputData(labels=categories, mccs=mcc_lists)


def categorize_mccs(dataset: Dataset) -> Categorized_MCCs_T:
    categorized_mccs = collections.defaultdict(collections.Counter)
    for transaction in dataset.transactions:
        categorized_mccs[transaction.trx_category][transaction.mcc] += 1

    return categorized_mccs


def random_color() -> str:
    res = '#'
    choices = '0123456789ABCDEF'
    for _ in range(6):
        res += random.choice(choices)
    return res


COLORS = [random_color() for _ in range(100)]


if __name__ == '__main__':
    main(dataset_file_path=pathlib.Path('transactions.csv'), your_birth_day=YOUR_BIRTH_DAY)
