# Guide

1. Make sure you have python3.6+ installed (only tested on 3.9 though) by running `python3 -V`
2. Run `pip3 install -r requirements.txt`
3. Configure your solution on line 17
4. Run `python3 solution.py`
